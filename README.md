# Tracker Plots

Repository for CMS Tracker plots 

## Introduction

This repository consists of Tracker plots that are to be displayed on the online webpage https://ecaldpgplots.web.cern.ch/#/tracker

The plots are stored in PNG and/or PDF format, along with a metadata file (`metadata.yaml`)that contains detailed information and relevant tags corresponding to the respective plots.  

## LFS

For images and pdf files (and other binary files) Git LFS is used. It prevents from fetching large set of files during `git clone`

git LFS Documentation https://about.gitlab.com/blog/2017/01/30/getting-started-with-git-lfs-tutorial/

## Installing git LFS

on lxplus: it is **already installed**

on debian-like OS:

```
apt-get install git-lfs
```

on centos-like OS:

```
yum install git-lfs
```

## Cloning the repository

After cloning the repository, to setup git LFS 

Run:

```bash
git lfs install --skip-smudge
```

This will update your local ~/.gitconfig with LFS settings. 
`--skip-smudge` prevents cloning of LSF objects.


If you want to fetch actual files use:

```bash
git lfs fetch --all
```

To fetch single file use

```bash
git lfs pull --exclude="" --include <path to file or filename>
```

For more documentation read https://github.com/git-lfs/git-lfs/blob/master/docs/man/git-lfs-fetch.1.ronn

## Contributing to the repository

Text files can be edited and pushed to the repository with the usual git commands. Same goes for pushing folders containing plots and metadata files. However, make sure that you have properly installed and set up git LFS using the commands above.

```bash
vim <file>
git add <file>
git commit <file>
git push
```

## Validating content

Before pushing your content to the remote,

Run:

```bash
python3 -m validate_content.py content
```
This will allow you to validate your content and check for any inconsistencies in your metadata. Once you push your content to the remote, it will trigger a rebuild of the webpage on which the plots are displayed. Therefore it is highly advisable to validate your content before pushing, to avoid any build errors.
